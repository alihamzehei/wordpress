<?php



function add_theme_scripts()
{
  wp_enqueue_style('bootstrap', get_template_directory_uri() .  '/assets/css/bootstrap.css');
  wp_enqueue_style('font-awesome', get_template_directory_uri() .  '/assets/css/font-awesome.css');
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');
